extern crate clap;
use clap::{Arg, App};

use std::net::{TcpStream};
use std::path::Path;

mod auth;
mod list;
mod sync;

fn main() {
    // argument declaration
    let matches = App::new("AFTP Client")
        .arg(Arg::with_name("a")
            .short("a")
            .help("Enables authentication"))
        .arg(Arg::with_name("FOLDER")
            .help("Sets the folder to use")
            .required(true)
            .index(1))
        .get_matches();

    // connect to server
    // TODO should be parameter
    let stream = TcpStream::connect("127.0.0.1:8000").expect("Could not connect to server");

    // define root_dir
    let folder = String::from(matches.value_of("FOLDER").expect("no folder argument found"));
    // canonicalize shortens path, removes doubles dots, etc.
    let root_dir = Path::new(&folder).canonicalize().expect("Could not canonicalize root_dir");

    // if flag is set, enable key exchange
    if matches.args.contains_key("a") {
        auth::send_auth(&stream);
    }

    // get list of files on server
    let optional_list = list::send_list(&stream);

    // handle list if it contains entries
    match optional_list {
        Some(list) => {
            sync::handle_list(&root_dir, &list, &stream);
        }
        None => {
            println!("No content!");
        }
    }
}